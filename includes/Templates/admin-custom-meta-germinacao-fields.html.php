<div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="planta-id" class="abst-row-title">Codigo da planta</label>
        </div>

        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_planta_id" id="planta-id" value="<?php if ( ! empty ( $post_id['_planta_id'] ) ) echo esc_attr( $post_id['_planta_id'][0] ) ?>">
        </div>
    </div>
    <div class="meta-row">
        <div class="meta-th">
            <label for="planta-data-germinacao" class="abst-row-title">Data de germinação</label>
        </div>

        <div class="meta-td">
            <input class="abst-row-content" type="date" name="_planta_data_germinacao" id="planta-data-germinacao" value="<?php if ( ! empty ( $post_id['_planta_data_germinacao'] ) ) echo esc_attr( $post_id['_planta_data_germinacao'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="planta-autorizado" class="abst-row-title">Autorizado</label>
        </div>

        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_planta_autorizado" id="planta-autorizado" value="<?php if ( ! empty ( $post_id['_planta_autorizado'] ) ) echo esc_attr( $post_id['_planta_autorizado'][0] ) ?>">
        </div>
    </div>
</div>