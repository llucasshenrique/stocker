<div>
    <div class="meta-row">
        <div class="meta-th">
            <label for="plantio-data" class="abst-row-title">Data de Plantio</label>
        </div>

        <div class="meta-td">
            <input class="abst-row-content datepicker" type="text" name="_plantio_data" id="plantio-data" value="<?php if ( ! empty ( $post_id['_plantio_data'] ) ) echo esc_attr( $post_id['_plantio_data'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="plantio-solo" class="abst-row-title">Tipo de solo</label>
        </div>

        <div class="meta-editor">
            <?php
            
            $content = '';
            if ( ! empty ( $post_id['_plantio_solo'] ) ) $content = esc_attr( $post_id['_plantio_solo'][0] );
            $editor_id = '_plantio_solo';
            $settings = array(
                'textarea_rows' => 8,
                'media_buttons' => false);
            wp_editor( $content, $editor_id, $settings );
            
            ?>
        </div>


    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="plantio-sexo" class="abst-row-title">Sexo</label>
        </div>

        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_plantio_sexo" id="plantio-sexo" value="<?php if ( ! empty ( $post_id['_plantio_sexo'] ) ) echo esc_attr( $post_id['_plantio_sexo'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="plantio-floracao" class="abst-row-title">Data de floração</label>
        </div>

        <div class="meta-td">
            <input class="abst-row-content datepicker" type="text" name="_plantio_floracao" id="plantio-floracao" value="<?php if ( ! empty ( $post_id['_plantio_floracao'] ) ) echo esc_attr( $post_id['_plantio_floracao'][0] ) ?>">
        </div>
    </div>


</div>
