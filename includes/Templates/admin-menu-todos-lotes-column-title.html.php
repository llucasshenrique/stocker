<a href="<?php echo $edit_url; ?>">
    <strong><?php echo $title; ?></strong>
</a>

<div class="row-actions">


    <span class="edit">
        <a href="<?php echo $edit_url; ?>"><?php echo 'Editar'; ?></a> |
    </span>


    <span class="trash">
        <a href="<?php echo $delete_url; ?>"><?php echo 'Deletar'; ?></a> |
    </span>

    <span class="duplicate">
        <a href="<?php echo $duplicate_url; ?>"><?php echo 'Duplicar'; ?></a> |
    </span>

    <span class="bleep">
        <a href="<?php echo $preview_url; ?>"><?php echo 'Ver Lote'; ?></a> |
    </span>


</div>