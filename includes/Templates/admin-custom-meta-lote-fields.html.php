<div>
    <div class="meta-row">
        <h2>Colheita</h2>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-id" class="abst-row-title">Data de Colheita</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content datepicker" type="text" name="_lote_id" id="lote-id" value="<?php if ( ! empty ( $post_id['_lote_id'] ) ) echo esc_attr( $post_id['_lote_id'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-peso" class="abst-row-title">Peso</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_peso" id="lote-peso" value="<?php if ( ! empty ( $post_id['_lote_peso'] ) ) echo esc_attr( $post_id['_lote_peso'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-peso-floracao" class="abst-row-title">Peso da floração no dia da coleta</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_peso_floracao" id="lote-peso-floracao" value="<?php if ( ! empty ( $post_id['_lote_peso_floracao'] ) ) echo esc_attr( $post_id['_lote_peso_floracao'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <h2>Secagem</h2>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-secagem-entrada" class="abst-row-title">Entrada</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content datepicker" type="text" name="_lote_secagem_entrada" id="lote-secagem-entrada" value="<?php if ( ! empty ( $post_id['_lote_secagem_entrada'] ) ) echo esc_attr( $post_id['_lote_secagem_entrada'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-secagem-saida" class="abst-row-title">Saída</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content datepicker" type="text" name="_lote_secagem_saida" id="lote-secagem-saida" value="<?php if ( ! empty ( $post_id['_lote_secagem_saida'] ) ) echo esc_attr( $post_id['_lote_secagem_saida'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <h2>Estocagem</h2>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-estocagem-data" class="abst-row-title">Data de estocagem</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content datepicker" type="text" name="_lote_estocagem_data" id="lote-estocagem-data" value="<?php if ( ! empty ( $post_id['_lote_estocagem_data'] ) ) echo esc_attr( $post_id['_lote_estocagem_data'][0] ) ?>">
        </div>
    </div>
    
    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-batch" class="abst-row-title">Batch</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_batch" id="lote-batch" value="<?php if ( ! empty ( $post_id['_lote_batch'] ) ) echo esc_attr( $post_id['_lote_batch'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-amostras" class="abst-row-title">Amostras</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_amostras" id="lote-amostras" value="<?php if ( ! empty ( $post_id['_lote_amostras'] ) ) echo esc_attr( $post_id['_lote_amostras'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-estocagem-local" class="abst-row-title">Local de estocagem</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_estocagem_local" id="lote-estocagem-local" value="<?php if ( ! empty ( $post_id['_lote_estocagem_local'] ) ) echo esc_attr( $post_id['_lote_estocagem_local'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-tecnico" class="abst-row-title">Técnico responsável</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_tecnico" id="lote-tecnico" value="<?php if ( ! empty ( $post_id['_lote_tecnico'] ) ) echo esc_attr( $post_id['_lote_tecnico'][0] ) ?>">
        </div>
    </div>

    <div class="meta-row">
        <div class="meta-th">
            <label for="lote-farmaceutico" class="abst-row-title">Farmacêutico responsável</label>
        </div>
        <div class="meta-td">
            <input class="abst-row-content" type="text" name="_lote_farmaceutico" id="lote-farmaceutico" value="<?php if ( ! empty ( $post_id['_lote_farmaceutico'] ) ) echo esc_attr( $post_id['_lote_farmaceutico'][0] ) ?>">
        </div>
    </div>

</div>