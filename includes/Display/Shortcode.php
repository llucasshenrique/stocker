<?php

final class ABST_Display_Shortcode{

    public $strains;
    public $display_strain;

    public function __construct() {
        
        add_shortcode( 'stocker', array( $this, 'display_stocker' ) );
    }

    public function display_stocker( $atts, $content = null ) {

        $atts = shortcode_atts( array(
            'strain'    => ''
        ), $atts
    );

    $strains = get_terms( 'strain' );

        if( ! empty( $strains ) && ! is_wp_error( $strains ) ) {
            $display = '<div id="lote-info-lis">';
            $display .= '<h4>'. $atts[ 'strain' ] . '</h4>';
            $display .= '<ul>';

            foreach( $strains as $strain) {

                $display .= '<li class="lote-title">';
                $display .= '<a href="' . get_term_link( $strain ). '">';
                $display .= $strain->_planta_id . '</a></li>';
            }
            $display .= '</ul></div>';
        }
    return $display;
    }
}