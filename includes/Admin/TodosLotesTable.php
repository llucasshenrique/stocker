<?php if ( ! defined( 'ABSPATH' ) ) exit;

if ( ! class_exists( 'WP_List_Table' ) )
{

    if ( file_exists( ABSPATH . 'wp-admi/includes/class-wp-list-table.php' ) )
    {

        require_once( ABSPATH . 'wp-admi/includes/class-wp-list-table.php' );
    } else {
                //TODO: Load local wp-list-table-class.php
    }
}

class ABST_Admin_TodosLotesTable extends WP_List_Table
{

    public function __construct()
    {

        parent::__construct( array(
            'singular'  => 'Lote',
            'plural'    => 'Lotes',
            'ajax'      => false
        ) );
    }

    public function no_items()
    {
        'Sem lotes';
    }

    /**
     * Prepare the items for the table to process
     *
     * @return Void
     */
    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $data = $this->table_data();
        usort( $data, array( &$this, 'sort_data' ) );

        $perPage = 20;
        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $perPage
        ) );

        $data = array_slice($data,(($currentPage-1)*$perPage),$perPage);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }

    public function get_columns()
    {
        $columns = array(
            'cb' => '<input type="checkbox"/>',
            'lote' => 'Lote',
            'tipo' => 'Tipo',
            'strain' => 'Strain'
        );
        return $columns;
    }

    /**
     * Define which columns are hidden
     *
     * @return Array
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * Define the sortable columns
     *
     * @return Array
     */
    public function get_sortable_columns()
    {
        return array(
            'lote' => array( 'lote', TRUE ),
            'strain'  => array( 'strain', TRUE ),
        );
    }

    private function table_data()     {
        $data = array();
        $lotes = array(
            array(
                'ID' => 1,
                'booktitle' => 'Quarter Share',
                'author' => 'Nathan Lowell',
                'isbn' => '978-0982514542' ),
            array(
                'ID' => 2,
                'booktitle' => '7th Son: Descent',
                'author' => 'J. C. Hutchins',
                'isbn' => '0312384378' ),
            array(
                'ID' => 3,
                'booktitle' => 'Shadowmagic',
                'author' => 'John Lenahan',
                'isbn' => '978-1905548927' ),
            array(
                'ID' => 4,
                'booktitle' => 'The Crown Conspiracy',
                'author' => 'Michael J. Sullivan',
                'isbn' => '978-0979621130' ),
            array(
                'ID' => 5,
                'booktitle'     => 'Max Quick: The Pocket and the Pendant',
                'author'    => 'Mark Jeffrey',
                'isbn' => '978-0061988929'),
            array(
                'ID' => 6,
                'booktitle' => 'Jack Wakes Up: A Novel',
                'author' => 'Seth Harwood',
                'isbn' => '978-0307454355' ) );
       foreach ( $lotes as $lote )
        {
            $data[] = array(
                'id' => 'lote',
                'booktitle' => 'tipo',
                'author' => 'strain' );
        }   
    return $data;
    }

    /**
     * Define what data to show on each column of the table
     *
     * @param  Array $item        Data
     * @param  String $column_name - Current column name
     *
     * @return Mixed
     */
    public function column_default( $item, $column_name )
    {
        switch( $column_name ) {
            case 'lote':
            case 'tipo':
            case 'strain':
                return $item[ $column_name ];

            default:
                return print_r( $item, true ) ;
        }
    }

    private function sort_data( $a, $b )
    {
        // Set defaults
        $orderby = 'id';
        $order = 'asc';

        // If orderby is set, use this as the sort column
        if(!empty($_GET['orderby']))
        {
            $orderby = $_GET['orderby'];
        }

        // If order is set use this as the order
        if(!empty($_GET['order']))
        {
            $order = $_GET['order'];
        }


        $result = strnatcmp( $a[$orderby], $b[$orderby] );

        if($order === 'asc')
        {
            return $result;
        }

        return -$result;
    }

        function column_cb( $item )
    {
        return sprintf(
            '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['id']
        );
    }

    function column_title( $item )
    {
        $lote = $item[ 'lote' ];
        $edit_url = add_query_arg( 'form_id', $item[ 'id' ], admin_url( 'admin.php?page=stocker') );
        $delete_url = add_query_arg( array( 'action' => 'delete', 'id' => $item[ 'id' ], '_wpnonce' => wp_create_nonce( 'abst_delete_form' )));
        $duplicate_url = add_query_arg( array( 'action' => 'duplicate', 'id' => $item[ 'id' ], '_wpnonce' => wp_create_nonce( 'abst_duplicate_form' )));
        $preview_url = add_query_arg( 'nf_preview_form', $item[ 'id' ], site_url() );
        $submissions_url = add_query_arg( 'form_id', $item[ 'id' ], admin_url( 'edit.php?post_status=all&post_type=nf_sub') );

       // $form = Ninja_Forms()->form( $item[ 'id' ] )->get();
       // $locked = $form->get_setting( 'lock' );

        Stocker::template( 'admin-menu-todos-lotes-column-title.html.php', compact( 'title', 'edit_url', 'delete_url', 'duplicate_url', 'preview_url', 'locked' ) );
        
    }

     /*
     * Returns an associative array containing the bulk action
     *
     * @return array
     */
    public function get_bulk_actions()
    {
        $actions = array(
            'bulk-delete' => 'Delete'
        );

        return $actions;
    }

    public static function process_bulk_action()
    {

    }
}