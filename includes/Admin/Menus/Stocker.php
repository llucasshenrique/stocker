<?php

final class ABST_Admin_Menus_Stocker extends ABST_Abstracts_Menu
{
    public $page_title = 'Stocker';

    public $menu_slug = 'stocker';

    public $icon_url = 'dashicons-media-spreadsheet';

    public $position = '36.9887';

    public function __construct()
    {
        parent::__construct();

            if( ! defined( 'DOING_AJAX' ) ) {
            add_action('admin_init', array($this, 'admin_init'));
            add_action( 'admin_init', array( 'ABST_Admin_TodosLotesTable', 'process_bulk_action' ) );
        }


    }

    public function admin_init()
    {
        $this->table = new ABST_Admin_TodosLotesTable();
    }

    public function get_page_title()
    {
        return $this->page_title;
    }

    public function get_capability()
    {
        return apply_filters( 'stocker_admin_parent_menu_capabilities', $this->capability );
    }

    public function display()
    {
        if( isset( $_GET[ 'lote_id' ] ) ){
            if( 'new' == $_GET[ 'lote_id' ] )
            {
                $lote_id = 'tmp-' . time();
            } else {
                $lote_id = ( is_numeric( $_GET[ 'lote_id' ] ) ) ? absint( $_GET['lote_id'] ) : '';
            }
            // FAZER: novo lote
        } else {
        $this->table->prepare_items();
        
        Stocker::template( 'admin-menu-todos-lotes.html.php', array( 
            'table'             => $this->table,
            'add_novo_url'      => admin_url( 'admin.php?page=stocker&lote_id=new' ),
            'add_novo_texto'    => 'Novo Lote'
            ) );
        }
    }
}