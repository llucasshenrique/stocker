<?php

final class ABST_Admin_Menus_Lote extends ABST_Abstracts_Submenu 
{
    public $parent_slug = 'stocker';

    public $menu_slug = 'admin.php?page=stocker';

    public $priority = 1;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_page_title()
    {
        return __( 'Lote', 'stocker' );
    }

    public function get_capability()
    {
        return $this->capability;
    }


    public function display()
    {
    }
}