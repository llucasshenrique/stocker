<?php

final class ABST_Admin_Menus_Entradas extends ABST_Abstracts_Submenu
{

    public $parent_slug = 'stocker';

    public $menu_slug = 'post-new.php?post_type=stocker';

    public $priority = 1;

    public function __construct()
    {
        parent::__construct();
    }

    public function get_page_title()
    {
        return __( 'Entrada', 'stocker' );
    }

    public function get_capability()
    {
        return $this->capability;
    }


    public function display()
    {

    }
}