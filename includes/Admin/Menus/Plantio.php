<?php

final class ABST_Admin_Menus_Plantio extends ABST_Abstracts_Submenu 
{
    public $parent_slug = 'stocker';

    public $menu_slug = 'plantio';

    public $priority = 13;

    public function __construct()
    {
        parent::__construct();

    }

    public function get_page_title()
    {
        return __( 'Plantio', 'stocker' );
    }

    public function get_capability()
    {
        return $this->capability;
    }


    public function display()
    {
     
    }

    
}