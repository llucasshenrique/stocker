<?php

class ABST_Admin_CPT_Entradas {

    protected $cpt_slug = 'abst_entrada';

    public $screen_options;

    public function __construct() {
        // Registra o Custom post type
        add_action( 'init', array( $this, 'custom_post_type' ), 5 );

        /* Chama os scripts e CSS
        *   Desativado para debug do datepicker e tradução.
        * add_action( 'admin_print_styles', array( $this, 'enqueue_scripts' ) );
        *
        */
        add_action( 'save_post', array( $this, 'meta_save' ) );
        // Adiciona as custom meta boxes
        add_action( 'add_meta_boxes', array( $this, 'custom_meta_box' ) );

        // Adiciona as custom taxonomy
        add_action( 'init', array( $this, 'custom_taxonomy' ) );
        
    }

    public function custom_post_type() {
        $labels = array(
            'name'               => _x( 'Stocker', 'post type general name', 'stocker' ),
            'singular_name'      => _x( 'Lote', 'post type singular name', 'stocker' ),
            'menu_name'          => _x( 'Stocker', 'admin menu', 'stocker' ),
            'name_admin_bar'     => _x( 'Lote', 'add new on admin bar', 'stocker' ),
            'add_new'            => _x( 'Adicionar Novo', 'stocker', 'stocker' ),
            'add_new_item'       => __( 'Adicionar Novo', 'stocker' ),
            'new_item'           => __( 'Novo Lote', 'stocker' ),
            'edit_item'          => __( 'Editar Lote', 'stocker' ),
            'view_item'          => __( 'Ver lote', 'stocker' ),
            'all_items'          => __( 'Lotes', 'stocker' ),
            'search_items'       => __( 'Procurar lotes', 'stocker' ),
            'parent_item_colon'  => __( 'Parent Lotes:', 'stocker' ),
            'not_found'          => __( 'Nenhum lote encontrado.', 'stocker' ),
            'not_found_in_trash' => __( 'Nenhum lote encontrado na lixeira.', 'stocker' ) );

        $args = array(
                'labels'                => $labels,
                'description'           => __( 'stocker', 'stocker' ),
                'public'                => false,
                'publicly_queryable'    => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'query_var'             => true,

                'capability_type'       => 'post',
                'has_archive'           => true,
                'hierarchical'          => false,
                'menu_position'         => null,
                'menu_icon'             => 'dashicons-media-spreadsheet',
                'rewrite'               => array(  
                                            'slug'          => 'stocker',
                                            'with_front'    => true,
                                            'pages'         => true,
                                            'feeds'         => false ),
                'supports'              => array(
                                            'title',  
                                            'custom_fields' ) );
        register_post_type( 'stocker', $args );
    }

    public function custom_meta_box() {
        add_meta_box(   'abst_meta_germinacao',
                        'Germinação', 
                        array( $this, 'render_germinacao' ), 
                        'stocker',
                        'normal'
                        );

        add_meta_box(   'abst_meta_plantio',
                        'Plantio', 
                        array( $this, 'render_plantio' ), 
                        'stocker',
                        'normal'
                        );
        add_meta_box(   'abst_meta_lote',
                        'Lote', 
                        array( $this, 'render_lote' ), 
                        'stocker',
                        'normal'
                        );
    }


    // As funções render chamam templates html contidas em /includes/templates/ a partir do metodo Classe_do_plugin::template() localizado no arquivo principal do plugin
    public function render_germinacao( $post ) {

        $post_id = get_post_meta( $post->ID );
        
        Stocker::template( 'admin-custom-meta-germinacao-fields.html.php', compact( 'post_id' ) );
        wp_nonce_field( 'abst_nonce', 'abst_metabox_germinacao' );
    }

    public function render_plantio( $post ) {

        $post_id = get_post_meta( $post->ID );

        Stocker::template( 'admin-custom-meta-plantio-fields.html.php', compact( 'post_id' ));
        wp_nonce_field( 'abst_nonce', 'abst_metabox_plantio' );
    }

    public function render_lote( $post ) {

        $post_id = get_post_meta( $post->ID );

        Stocker::template( 'admin-custom-meta-lote-fields.html.php', compact( 'post_id' ));
        wp_nonce_field( 'abst_nonce', 'abst_metabox_lote' );
    }

    /*
    *   Desativado para correção do datepicker Utilizar essa função para chamar jquery e css se necessário
    * Os scripts e CSS necessários para o funcionamento são chamados dentro desse metodo
    * Para scripts usa a função do wordpress wp_enqueue_script()
    * Para CSS wp_enqueue_style()
    */
    /*public function enqueue_scripts() {
        global $pagenow, $typenow;

        /*
        *
        * Sai se não estiver em edit.php, post-new.php ou não estiver editando seu custom type.
        * Verifica as globais $pagenow e $typenow para saber se são diferentes das paginas especificadas
        *
        *
        if ( ( $pagenow != 'edit.php' && $pagenow != 'post-new.php' && $pagenow != 'post.php') || $typenow != 'stocker' )
            return false;

        wp_enqueue_script( 'jquery',
            Stocker::$url . 'includes/assets/js/jquery-ui.js',
            array( 'jquery', 'jquery-ui' ), '1.12.1', false  );
        wp_enqueue_script( 'lote-cpt-datepicker',
            Stocker::$url . 'includes/assets/js/stocker-datepicker.js',
            array( 'jquery', 'jquery-ui-datepicker' ), '1.30.0', false );
        wp_localize_script( 'jquery',
            Stocker::$url . 'includes/assets/js/stocket-datepicker-tradutor.js',
            array( 'jquery', 'jquery-ui' ), '1.30.1', false  );

        wp_enqueue_style( 'jquery-style', Stocker::$url . 'includes/assets/js/jquery-ui.min.css' );


    }    */
    
    public function meta_save( $post_id ) {
        global $pagenow, $typenow;

        $is_autosave = wp_is_post_autosave( $post_id );
        $is_revision = wp_is_post_revision( $post_id );

        $is_valid_nonce = ( isset( $_POST[ 'abst_nonce' ] ) && wp_verify_nonce( $_POST[ 'abst_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

        if ( $is_autosave || $is_revision || ! $is_valid_nonce ) 
        {
            return;
        }
        //germinacao
        if ( isset( $_POST[ '_planta_id' ] ) ) {
    	update_post_meta( $post_id, '_planta_id', sanitize_text_field( $_POST[ '_planta_id' ] ) );
        }

        if ( isset( $_POST[ '_planta_data_germinacao' ] ) ) {
    	update_post_meta( $post_id, '_planta_data_germinacao', sanitize_text_field( $_POST[ '_planta_data_germinacao' ] ) );
        }

        if ( isset( $_POST[ '_planta_autorizado' ] ) ) {
    	update_post_meta( $post_id, '_planta_autorizado', sanitize_text_field( $_POST[ '_planta_autorizado' ] ) );
        }
        //plantio 
        if ( isset( $_POST[ '_plantio_data' ] ) ) {
    	update_post_meta( $post_id, '_plantio_data', sanitize_text_field( $_POST[ '_plantio_data' ] ) );
        }
        
        if ( isset( $_POST[ '_plantio_solo' ] ) ) {
    	update_post_meta( $post_id, '_plantio_solo', sanitize_text_field( $_POST[ '_plantio_solo' ] ) );
        }
        
        if ( isset( $_POST[ '_plantio_sexo' ] ) ) {
    	update_post_meta( $post_id, '_plantio_sexo', sanitize_text_field( $_POST[ '_plantio_sexo' ] ) );
        }
        
        if ( isset( $_POST[ '_plantio_floracao' ] ) ) {
    	update_post_meta( $post_id, '_plantio_floracao', sanitize_text_field( $_POST[ '_plantio_floracao' ] ) );
        }
        //lote

        if ( isset( $_POST[ '_lote_id' ] ) ) {
    	update_post_meta( $post_id, '_lote_id', sanitize_text_field( $_POST[ '_lote_id' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_peso' ] ) ) {
    	update_post_meta( $post_id, '_lote_peso', sanitize_text_field( $_POST[ '_lote_peso' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_peso_floracao' ] ) ) {
    	update_post_meta( $post_id, '_lote_peso_floracao', sanitize_text_field( $_POST[ '_lote_peso_floracao' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_secagem_entrada' ] ) ) {
    	update_post_meta( $post_id, '_lote_secagem_entrada', sanitize_text_field( $_POST[ '_lote_secagem_entrada' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_secagem_saida' ] ) ) {
    	update_post_meta( $post_id, '_lote_secagem_saida', sanitize_text_field( $_POST[ '_lote_secagem_saida' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_secagem_saida' ] ) ) {
    	update_post_meta( $post_id, '_lote_secagem_saida', sanitize_text_field( $_POST[ '_lote_secagem_saida' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_estocagem_data' ] ) ) {
    	update_post_meta( $post_id, '_lote_estocagem_data', sanitize_text_field( $_POST[ '_lote_estocagem_data' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_batch' ] ) ) {
    	update_post_meta( $post_id, '_lote_batch', sanitize_text_field( $_POST[ '_lote_batch' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_amostras' ] ) ) {
    	update_post_meta( $post_id, '_lote_amostras', sanitize_text_field( $_POST[ '_lote_amostras' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_estocagem_local' ] ) ) {
    	update_post_meta( $post_id, '_lote_estocagem_local', sanitize_text_field( $_POST[ '_lote_estocagem_local' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_tecnico' ] ) ) {
    	update_post_meta( $post_id, '_lote_tecnico', sanitize_text_field( $_POST[ '_lote_tecnico' ] ) );
        }
        
        if ( isset( $_POST[ '_lote_farmaceutico' ] ) ) {
    	update_post_meta( $post_id, '_lote_farmaceutico', sanitize_text_field( $_POST[ '_lote_farmaceutico' ] ) );
        }
        

    }

    public function custom_taxonomy() {
        $singular = 'Strain';
        $plural = 'Strains';


        $labels = array(
            'name'              => _x( $plural, 'taxonomy general name', 'stocker' ),
            'singular_name'     => _x( $singular, 'taxonomy singular name', 'stocker' ),
            'search_items'      => __( 'Search ' . $plural, 'stocker' ),
            'all_items'         => __( 'All ' . $plural, 'stocker' ),
            'parent_item'       => __( 'Parent ' . $singular, 'stocker' ),
            'parent_item_colon' => __( 'Parent ' . $singular . ':', 'stocker' ),
            'edit_item'         => __( 'Edit ' . $singular, 'stocker' ),
            'update_item'       => __( 'Update ' . $singular, 'stocker' ),
            'add_new_item'      => __( 'Add New ' . $singular, 'stocker' ),
            'new_item_name'     => __( 'New ' . $singular . ' Name', 'stocker' ),
            'menu_name'         => __( $singular, 'stocker' ),
            );
        $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'strain' ),
	);
        register_taxonomy( 'strain', 'stocker', $args );
    }
}