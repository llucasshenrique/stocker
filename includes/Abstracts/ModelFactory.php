<?php

abstract class ABST_Abstracts_ModelFactory
{
/**
     * Database Object
     *
     * @var
     */
    protected $_db;

    /**
     * The last set object.
     *   Used to create context between two objects in a chain.
     *
     * @var object
     */
    protected $_object;

    /**
     * Fields
     *
     * An array of field model objects.
     *
     * @var array
     */
    protected $_fields = array();

    /**
     * Actions
     *
     * An array of action model objects.
     *
     * @var array
     */
    protected $_actions = array();

    /**
     * Objects
     *
     * An array of generic model objects.
     *
     * @var array
     */
    protected $_objects = array();

    
}