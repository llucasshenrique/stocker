<?php

abstract class ABST_Abstracts_Menu
{
    public $page_title = '';

    public $menu_title = '';

    public $capability = 'manage_options';

    public $menu_slug = '';

    public $function = 'display';

    public $icon_url = '';

    public $position = NULL;

    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'register' ) );
    }

    public function register()
    {
        add_menu_page( 
            $this->get_page_title(),
            $this->get_menu_title(),
            $this->get_capability(),
            $this->get_menu_slug(),
            array( $this, $this->function),
            $this->icon_url,
            $this->position
        );
    }

    public function get_page_title()
    {
        return $this->page_title;
    }

    public function get_menu_title()
    {
        return ( $this->menu_title ) ? $this->menu_title : $this->get_page_title();
    }

    public function get_capability()
    {
        return $this->capability;
    }

    public function get_menu_slug()
    {
        return $this->menu_slug;
    }



}