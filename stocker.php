<?php
/*
PLUGIN NAME: Stocker
PLUGIN URI:
VERSION: 0.1a
AUTHOR: Lucas
DESCRIPTION: Sistema de estoque e lotes.
*/

final class Stocker {
    public static $instance;

    public static $dir = '';

    public static $url = '';

    public $menus = array();


    /* Instacia o a classe do plugin e carrega as outras classes necessárias para seu funcionamento */
    public static function instance() {
        if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Stocker ) ) {
            self::$instance = new Stocker;

            self::$dir = plugin_dir_path( __FILE__ );
            if( ! defined( 'ABST_PLUGIN_DIR' ) ) {
                    define( 'ABST_PLUGIN_DIR', self::$dir );
                    define( 'ABRACE_STOCKER_DIR', self::$dir . 'deprecated' );
                }

            self::$url = plugin_dir_url( __FILE__ );
            if( ! defined( 'ABST_PLUGIN_URL' ) ) {
                    define( 'ABST_PLUGIN_URL', self::$url );
                }
            /*
            * Chama as classes em arrays e salva os arrays em $instance
            * Dentro da pasta include fica as pastas com as classes que serão instanciadas a seguir
            * a estrutura do nome de classe segue a seguinte logica
            * Prefixo do plugin 'exemplo_', pastaraiz_pastafilha_pastaneta_nomedoarquivo
            * isso é necessario para que a função autoloader() funcione
            */

            // Registra o autoloader para as classes do plugin
            spl_autoload_register( array( self::$instance, 'autoloader' ) );

            //Classes de funcionalidades do plugin

            //Menu de administração

            /* Desativado para correção de atalhos no menu de administração

            self::$instance->menus[ 'stocker' ] = new ABST_Admin_Menus_Stocker();
            self::$instance->menus[ 'lote' ] = new ABST_Admin_Menus_Lote();
            self::$instance->menus[ 'plantio' ] = new ABST_Admin_Menus_Plantio();
            self::$instance->menus[ 'entradas' ] = new ABST_Admin_Menus_Entradas();
            */
            new ABST_Admin_CPT_Entradas();

            self::$instance->shortcodes = new ABST_Display_Shortcode();
        }

        add_action( 'int', array( self::$instance, 'init' ), 5  );
        add_action( 'admin_init', array( self::$instance, 'admin_init' ), 5 );

        return self::$instance;
    }

    public function init()
    {
        do_action( 'abst_init', self::$instance );
    }
    public function admin_init()
    {
        do_action( 'abst_admin_init', self::$instance );
    }
    
    /*
    *
    * autoloader() procura o arquivo das classes instanciadas em instance()
    * seguindo a estrura de nome de classe citada em instance()
    *
    */
    public function autoloader( $class_name )
    {
        if( class_exists( $class_name ) ) return;
        /*
        * Verifica se o prefixo do plugin existe no nome da classe
        */
        if (false !== strpos( $class_name, 'ABST_') )
        {
            // troca o 'prefixo_' por '', vazio, retirando-o do nome da classe
            $class_name     = str_replace( 'ABST_', '', $class_name);
           
            // declara $classes_dir como sendo o caminho real para o diretorio do plugin /includes/
            $classes_dir    = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
           
            // troca todos os underlines '_' por barras '/' no $class_name e adiciona o '.php' ao final
            $class_file     = str_replace( '_', DIRECTORY_SEPARATOR, $class_name) . '.php';
            
            // procura pelo arquivo da classe e se existir o requere uma vez
            if ( file_exists( $classes_dir . $class_file ) )
            {
                require_once $classes_dir . $class_file;
            }
        }
    }

    //Essa classe carrega as tamplates que serão mostradas dentro dos menus
    public static function template( $file_name='', array $data = array(), $return = false )
    {
        if ( ! $file_name ) return false;

        extract( $data );

        $path = self::$dir . 'includes/Templates/' . $file_name;

        if ( ! file_exists( $path ) ) return false;

        if ( $return ) return file_get_contents( $path );

        include ( $path );

    }

    //Essa função ainda não é utilizada
    private static function load_classes( $prefix='' )
    {
        $return = array();

        $subdirectory = str_replace( '_', DIRECTORY_SEPARATOR, str_replace( 'ABST_', '', $prefix ) );

        $directory = 'includes/' . $subdirectory;

        foreach (scandir( self::$dir . $directory ) as $path )
        {
            $path = explode( DIRECTORY_SEPARATOR, str_replace( self::$dir, '', $path ) );
            $filename = str_replace( '.php', '', end( $path ) );

            $class_name = 'ABST_' . $prefix . '_' . $filename;

            if( ! class_exists( $class_name ) ) continue;

            $return[ strtolower( $filename ) ] = new $class_name;
        }

        return $return;
    }



    /*
    * PUBLIC API WRAPPERS
    */

    /**
    * Form Model Factory Wrapper
    *
    * @param $id
    * @return NF_Abstracts_ModelFactory
    */
    public function lote( $id = '' )
    {
        global $wpdb;

        return new ABST_Abstracts_ModelFactory( $wpdb, $id );
    }
}

function Stocker()
{
    return Stocker::instance();
}

Stocker();